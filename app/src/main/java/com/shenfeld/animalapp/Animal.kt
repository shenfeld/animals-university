package com.shenfeld.animalapp

enum class Animal(val id: Int, val title: String) {
    BEAR(R.drawable.bear, "Bear"),
    CAT(R.drawable.cat, "Cat"),
    RACCOON(R.drawable.raccoon, "Raccoon"),
    FOX(R.drawable.fox, "Fox"),
    RABBIT(R.drawable.rabbit, "Rabbit"),
    WOLF(R.drawable.wolf, "Wolf"),
    TIGER(R.drawable.tiger, "Tiger"),
    LION(R.drawable.lion, "King Lion"),
    PANDA(R.drawable.panda, "Panda"),
    SQUIRREL(R.drawable.squirrel, "Squirrel");
}