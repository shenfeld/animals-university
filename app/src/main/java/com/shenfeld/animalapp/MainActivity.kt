package com.shenfeld.animalapp

import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat

class MainActivity : AppCompatActivity() {
    private lateinit var btnGetNextAnimal: Button
    private lateinit var ivAnimal: ImageView
    private lateinit var tvNameAnimal: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupView()
        setupClickListeners()
    }

    private fun setupView() {
        btnGetNextAnimal = findViewById(R.id.btnGetNextAnimal)
        ivAnimal = findViewById(R.id.ivAnimal)
        tvNameAnimal = findViewById(R.id.tvNameAnimal)
    }

    private fun setupClickListeners() {
        btnGetNextAnimal.setOnClickListener {
            val nextAnimal = Animal.values().random()
            setNextAnimal(nextAnimal)
        }
    }

    private fun setNextAnimal(nextAnimal: Animal) {
        ivAnimal.setImageResource(nextAnimal.id)
        tvNameAnimal.text = nextAnimal.title
    }
}